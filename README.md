Simple eShop
===

Instalation
---
* You will need to install [composer](https://getcomposer.org/download/) components. To do so run command `composer install`. There you should been asked to fill database connection params. If you did that just skip next step. Also if you haven't created database at the end there should be some errors with message "unknown database". Nothing to worry about, you will create database in step 3.
* Go to `app/config/parameters` and fill database connection params
* Then, if not yet created, create database by executing command `php bin/console doctrine:database:create`
* And create schema `php bin/console doctrine:schema:update --force`
* Since you have the database and schema, import predefined data by executing command `php bin/console app:migrate`
* Make styles, javascripts visible for browsers: `php bin/console assets:install`.
* Then start server by executing command `php bin/console server:run`
* That's it. Simple eShop started and is reachable at the address that shows after executing previous step (most likely [http://127.0.0.1:8000](http://127.0.0.1:8000))

Summary
---
Create a small web application that must implement:

* User system
    * User can register
    * User can login
    * Logged user can change his password
    * User can logout
* User Items
    * Logged user can add items they want to sell they want to sell with price, description and picture(s)
    * Logged user can assign predefined category(ies) for the item
    * Logged user can add items to they shoping cart
* User cart
    * Display list of items
    * Total price
    * When user press Buy button in Cart a
        * If user is not logged in - ask to log in or create account and after that proceed buy process
        * create buy request(s) for user(s) who sells the items with items that user who pressed the buy button is interested buying
* In frontend list of all users selling items
    * Posibility to order by latest items
    * Posibility to order by price (highest, lowest)
    * Filter by predefined categories
    * When clicked on item a page with more detailed information is opened

Test was done by
---
Mantas Janonis

+370 689 78844

man.janonis@gmail.com
