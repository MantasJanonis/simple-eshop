<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AppMigrateCommand extends ContainerAwareCommand
{
    protected $importFileName = __DIR__ . '/../Resources/migration/predefined.sql';

    protected function configure()
    {
        $this
            ->setName('app:migrate')
            ->setDescription('...')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Starting migration');

        $sql = file_get_contents($this->importFileName);
        $con = $this->getContainer()->get('doctrine.orm.entity_manager')->getConnection()->prepare($sql);
        $con->execute();

        $output->writeln('Migration completed. Predefined data migrated.');
    }

}
