<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * Parameters that will be set to twig
     *
     * @var array
     */
    protected $viewParams = [];

    /**
     * Had no idea what to display in homepage, so just hardcoded to display 3 newest products.
     *
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $order = [
            'orderBy' => 'created',
            'orderDir' => 'DESC',
        ];

        $this->viewParams['products'] = $this->get('app.product')->getProducts(null, $order, 3);
        $this->viewParams['productsPage'] = $this->generateUrl('products_list');

        return $this->render('default/index.html.twig', $this->viewParams);
    }
}
