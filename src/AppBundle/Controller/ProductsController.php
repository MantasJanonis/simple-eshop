<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Form\ProductType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;

class ProductsController extends Controller
{
    /**
     * @var array
     */
    protected $viewParams = [];

    /**
     * @Route("/products/{page}", name="products_list")
     */
    public function productsListAction(Request $request, $page = 0)
    {
        $findBy = $this->getProductsFilter($request);
        $orderBy = $this->getOrder($request);
        $limit = $this->getPerPage($request);
        $offset = $this->getOffset($request, $page);

        $this->viewParams['pagination'] = $this->get('app.service.pagination')->createPagination(
            $this->get('app.product')->getProductsCount($findBy),
            $limit,
            $page
        );

        $this->viewParams['possibleSorts'] = $this->getPossibleSorts();
        $this->viewParams['categories'] = $this->get('app.category')->getCategories();
        $this->viewParams['products'] = $this->get('app.product')->getProducts($findBy, $orderBy, $limit, $offset);

        if ($request->query->get('activeSort')) {
            $this->viewParams['activeSort'] = $this->get('app.config')->getConfig($request->query->get('activeSort'));
        }
        if ($request->query->get('activeCategory')) {
            $this->viewParams['activeCategory'] = $this->get('app.category')->getCategory($request->query->get('activeCategory'));
        }

        return $this->render('products/products_list.html.twig', $this->viewParams);
    }

    /**
     * @Route("/my-products", name="my_products")
     */
    public function myProductsAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $this->viewParams['products'] = $user->getProducts();
        $this->viewParams['categories'] = $this->get('app.category')->getCategories();

        return $this->render('products/my_products.html.twig', $this->viewParams);
    }

    /**
     * @Route("/create-product", name="create_product")
     */
    public function createProductAction()
    {
        $product = $this->get('app.product')->getEmptyProduct();
        $this->viewParams['productForm'] = $this->createForm(ProductType::class, $product)->createView();
        $this->viewParams['categories'] = $this->get('app.category')->getCategories();

        return $this->render('products/create_product.html.twig', $this->viewParams);
    }

    /**
     * @Route("/save-product", name="save_product")
     */
    public function saveProductAction(Request $request)
    {
        $product = $this->get('app.product')->getEmptyProduct();
        $form = $this->createForm('AppBundle\Form\ProductType', $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $product->setUser($this->getUser());
            $product = $this->get('app.product')->handleNewProduct($product);
            $this->viewParams['product'] = $product;

            return $this->render('products/product_created.html.twig', $this->viewParams);
        }

        $this->viewParams['productForm'] = $form->createView();
        $this->viewParams['categories'] = $this->get('app.category')->getCategories();

        return $this->render('products/create_product.html.twig', $this->viewParams);
    }

    /**
     * @Route("/create-product/{id}", name="details")
     */
    public function detailsAction($id)
    {
        $this->viewParams['product'] = $this->get('app.product')->getProduct($id);
        $this->viewParams['categories'] = $this->get('app.category')->getCategories();

        return $this->render('products/details.html.twig', $this->viewParams);
    }

    /**
     * @param Request $request
     * @return array|null
     */
    protected function getProductsFilter(Request $request)
    {
        if (empty($request->query->get('activeCategory'))) {
            return null;
        }

        return [
            'findField' => 'category',
            'findValue' => $request->query->get('activeCategory')
        ];
    }

    /**
     * @param Request $request
     * @return array|null
     */
    protected function getOrder(Request $request)
    {
        if (empty($request->query->get('activeSort'))) {
            return null;
        }

        $activeSort = $this->get('app.config')->getConfig($request->query->get('activeSort'));
        $activeSort = explode(' ', $activeSort->getValue());

        return [
            'orderBy' => $activeSort[0],
            'orderDir' => $activeSort[1]
        ];
    }

    /**
     * TODO implement products per page (not necessary for now)
     *
     * @return int
     */
    protected function getPerPage(Request $request)
    {
        return 10;
    }

    /**
     * @param int $page
     * @return int
     */
    protected function getOffset(Request $request, $page)
    {
        return $page * $this->getPerPage($request);
    }

    /**
     * @return \AppBundle\Entity\Config|null
     */
    protected function getPossibleSorts()
    {
        return $this->get('app.config')->getConfigByType('sorting');
    }
}
