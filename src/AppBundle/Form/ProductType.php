<?php

namespace AppBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array('label' => 'product.create.title'))
            ->add('description', TextareaType::class, array('label' => 'product.create.description'))
            ->add('price', MoneyType::class, array('label' => 'product.create.price'))
            ->add('amount', IntegerType::class, array('label' => 'product.create.amount'))
            ->add('category', EntityType::class, array(
                'class' => 'AppBundle\Entity\Category',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.position', 'ASC');
                },
                'label' => 'product.create.category',
                'choice_label' => 'title'
            ))
            ->add('tempPictures', FileType::class, array(
                'multiple' => true,
                'required' => false
            ))

        ;
    }
}
