<?php

namespace AppBundle\Model;

use Doctrine\ORM\EntityManager;

class CategoryModel
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function getCategory($id)
    {
        return $this->em->getRepository('AppBundle:Category')->find($id);
    }

    public function getCategories()
    {
        $qb = $this->em->createQueryBuilder();

        $questions = $qb->select('p')
            ->from('AppBundle:Category', 'p')
            ->orderBy('p.position', 'ASC')
            ->getQuery()->getResult();

        return $questions;
    }
}
