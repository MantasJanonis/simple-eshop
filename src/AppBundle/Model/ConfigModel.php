<?php

namespace AppBundle\Model;

use Doctrine\ORM\EntityManager;

class ConfigModel
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param $name
     * @return \AppBundle\Entity\Config|null
     */
    public function getConfig($name)
    {
        return $this->em->getRepository('AppBundle:Config')->findOneBy(['name' => $name]);
    }

    /**
     * @param $type
     * @return \AppBundle\Entity\Config|null
     */
    public function getConfigByType($type)
    {
        return $this->em->getRepository('AppBundle:Config')->findBy(['type' => $type], ['position' => 'ASC']);
    }
}
