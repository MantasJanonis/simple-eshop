<?php

namespace AppBundle\Model;

use AppBundle\Entity\Product;
use AppBundle\Entity\Category;
use UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ProductModel
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var string
     */
    protected $picturesPath;

    /**
     * @var string
     */
    protected $webPicturesPath;

    public function __construct(EntityManager $entityManager, $picturesPath, $webPicturesPath)
    {
        $this->em = $entityManager;
        $this->picturesPath = $picturesPath;
        $this->webPicturesPath = $webPicturesPath;
    }

    public function getProduct($id)
    {
        return $this->em->getRepository('AppBundle:Product')->find($id);
    }

    /**
     * @param null|array $findBy array('findField' => 'fieldName', 'findValue' => string)
     * @param null|array $orderBy array('orderBy' => 'fieldName', 'orderDir' => 'ASC|DESC')
     * @param null|int $limit
     * @param null|int $offset
     * @return \AppBundle\Entity\Product[]|array
     */
    public function getProducts($findBy = null, $orderBy = null, $limit = null, $offset = null)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('p')
            ->from('AppBundle:Product', 'p');

        if (isset($findBy)) {
            $qb->where($qb->expr()->eq('p.' . $findBy['findField'], $findBy['findValue']));
        }
        if (isset($orderBy)) {
            $qb->orderBy('p.' . $orderBy['orderBy'], $orderBy['orderDir']);
        }
        if (isset($offset)) {
            $qb->setFirstResult($offset);
        }
        if (isset($limit)) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }

    public function getProductsByIds($ids)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('p')
            ->from('AppBundle:Product', 'p')
            ->where($qb->expr()->in('p.id', $ids));

        return $qb->getQuery()->getResult();
    }

    public function getProductsCount($findBy = null)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select($qb->expr()->count('p'))
            ->from('AppBundle:Product', 'p');

        if (isset($findBy)) {
            $qb->where($qb->expr()->eq('p.' . $findBy['findField'], $findBy['findValue']));
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Returns if product exists
     *
     * @param $id
     * @return bool
     */
    public function doesProductExist($id)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select($qb->expr()->count('p'))
            ->from('AppBundle:Product', 'p')
            ->where($qb->expr()->eq('p.id', $id));

        return (bool) $qb->getQuery()->getSingleScalarResult();
    }

    public function getEmptyProduct()
    {
        return new Product();
    }

    /**
     * @param Product $product
     * @return Product
     */
    public function handleNewProduct($product)
    {
        $pictures = $this->handlePictures($product->getTempPictures());
        $product->setPictures($pictures);

        $this->em->persist($product);
        $this->em->flush();

        return $product;
    }

    /**
     * @param array $pictures
     * @return array $savedPictures
     */
    protected function handlePictures($pictures)
    {
        $savedPictures = [];

        foreach ($pictures as $picture) {
            /** @var UploadedFile $picture */
            $pictureName = time() . '_' . $picture->getClientOriginalName();
            $savedPicture = $picture->move($this->picturesPath, $pictureName);
            $savedPictures[] = $this->webPicturesPath . $savedPicture->getBasename();
        }

        return $savedPictures;
    }
}
