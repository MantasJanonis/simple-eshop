/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Data for the table `category` */

insert  into `category`(`id`,`title`,`position`) values (1,'Video cards',1);
insert  into `category`(`id`,`title`,`position`) values (2,'Processors',2);
insert  into `category`(`id`,`title`,`position`) values (3,'Motherboards',3);
insert  into `category`(`id`,`title`,`position`) values (4,'PSU',4);
insert  into `category`(`id`,`title`,`position`) values (5,'HDD/SSD',5);
insert  into `category`(`id`,`title`,`position`) values (6,'RAM',6);
insert  into `category`(`id`,`title`,`position`) values (7,'Coolers',7);

/*Data for the table `config` */

insert  into `config`(`id`,`name`,`type`,`display_name`,`value`,`position`) values (1,'latestSort','sorting','Latest','created DESC',1);
insert  into `config`(`id`,`name`,`type`,`display_name`,`value`,`position`) values (2,'priceSortAsc','sorting','Price 1 - 100','price ASC',2);
insert  into `config`(`id`,`name`,`type`,`display_name`,`value`,`position`) values (3,'priceSortDesc','sorting','Price 100 - 1','price DESC',3);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
