<?php

namespace AppBundle\Utils;

class Pagination implements PaginationInterface
{
    /**
     * @param $totalProducts
     * @param $perPage
     * @param $page
     * @return array
     */
    public function createPagination($totalProducts, $perPage, $page)
    {
        $totalPages = ceil($totalProducts / $perPage);
        $pages = [];

        $pages[] = $this->getPrevPage($totalPages, $page, $page - 1);
        for ($i = 0; $i < $totalPages; $i++) {
            $pages[] = $this->getPage($totalPages, $page, $i);
        }
        $pages[] = $this->getNextPage($totalPages, $page, $page + 1);

        return $pages;
    }

    protected function getPrevPage($totalPages, $currentPage, $destinationPage)
    {
        $page = $this->getPage($totalPages, $currentPage, $destinationPage);
        $page->value = '&laquo;';

        return $page;
    }

    protected function getNextPage($totalPages, $currentPage, $destinationPage)
    {
        $page = $this->getPage($totalPages, $currentPage, $destinationPage);
        $page->value = '&raquo;';

        return $page;
    }

    protected function getPage($totalPages, $currentPage, $destinationPage)
    {
        $page = new \stdClass();
        $page->class = $this->getClass($totalPages, $currentPage, $destinationPage);
        $page->disabled = $this->getIsDisabled($destinationPage, $totalPages);
        $page->page = $destinationPage;
        $page->value = $destinationPage + 1;

        return $page;
    }

    protected function getClass($totalPages, $currentPage, $destinationPage)
    {
        if ($this->getIsActive($destinationPage, $currentPage)) {
            return 'active';
        } elseif ($this->getIsDisabled($destinationPage, $totalPages)) {
            return 'disabled';
        }

        return '';
    }

    protected function getIsDisabled($destinationPage, $totalPages)
    {
        if ($destinationPage < 0 || $destinationPage >= $totalPages) {
            return true;
        }

        return false;
    }

    protected function getIsActive($destinationPage, $currentPage)
    {
        if ($destinationPage == $currentPage) {
            return true;
        }

        return false;
    }
}
