<?php

namespace AppBundle\Utils;

interface PaginationInterface
{
    /**
     * @param $totalProducts
     * @param $perPage
     * @param $page
     * @return array
     */
    public function createPagination($totalProducts, $perPage, $page);
}