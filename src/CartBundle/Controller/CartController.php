<?php

namespace CartBundle\Controller;

use AppBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CartController extends Controller
{
    /**
     * @var array
     */
    protected $viewData = [];

    /**
     * @return Response
     */
    public function myCartAction()
    {
        $cart = $this->get('session')->get('cart');
        if (!empty($cart)) {
            $this->viewData['products'] = $this->get('app.product')->getProductsByIds($cart);
            $this->viewData['cartPrice'] = $this->get('cart.service.price_calculator')
                ->calculateTotalPrice($this->viewData['products']);
        }

        return $this->render('CartBundle::my_cart.html.twig', $this->viewData);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function addToCartAction(Request $request)
    {
        $success = $this->addToCart($request->request->get('productId'));

        return new JsonResponse(['added' => $success]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeFromCartAction(Request $request)
    {
        $this->removeFromCart($request->query->get('productId'));

        return $this->redirect($this->generateUrl('my_cart'));
    }

    /**
     * @return Response
     */
    public function cartIconAction()
    {
        $this->viewData['cart'] = $this->get('session')->get('cart');

        return $this->render('CartBundle::cart_icon.html.twig', $this->viewData);
    }

    /**
     * @return Response
     */
    public function createOrderAction()
    {
        $user = $this->getUser();
        $cart = $this->get('session')->get('cart');

        if (empty($cart)) {
            $this->viewData['message'] = 'cart.order.empty_cart';
            $this->viewData['status'] = 'warning';
        } else {
            $products = $this->get('app.product')->getProductsByIds($cart);

            if ($this->get('cart.order')->createOrder($user, $products)) {
                $this->clearCart();
                $this->viewData['message'] = 'cart.order.success';
                $this->viewData['status'] = 'success';
            } else {
                $this->viewData['message'] = 'cart.order.fail';
                $this->viewData['status'] = 'danger';
            }
        }

        return $this->render('CartBundle::order_confirmed.html.twig', $this->viewData);
    }

    public function buyRequestsAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new Response();
        }

        $this->viewData['buyRequestCount'] = $this->get('cart.order')->getSellerOrdersCount($this->getUser());

        return $this->render('CartBundle::buy_requests.html.twig', $this->viewData);
    }

    public function myBuyRequestsAction()
    {
        $this->viewData['orders'] = $this->get('cart.order')->getSellerOrders($this->getUser());

        return $this->render('CartBundle::my-buy-requests.html.twig', $this->viewData);
    }

    protected function clearCart()
    {
        $this->get('session')->remove('cart');
    }

    /**
     * @param int $productId
     * @return bool
     */
    protected function addToCart($productId)
    {
        $cart = $this->get('session')->get('cart');

        if ($this->isValidProduct($cart, $productId)) {
            $cart[] = $productId;
            $this->get('session')->set('cart', $cart);

            return true;
        }

        return false;
    }

    /**
     * @param int $productId
     * @return bool
     */
    protected function removeFromCart($productId)
    {
        $cart = $this->get('session')->get('cart');

        foreach ($cart as $key => $product) {
            if ($productId == $product) {
                unset($cart[$key]);
                $this->get('session')->set('cart', $cart);
                return true;
            }
        }

        return false;
    }

    /**
     * @param $cart
     * @param int $productId
     * @return bool
     */
    protected function isValidProduct($cart, $productId)
    {
        if (!$this->get('app.product')->doesProductExist($productId)) {
            return false;
        }

        if (empty($cart)) {
            return true;
        }

        if ($this->isAlreadyInBasket($cart, $productId)) {
            return false;
        }

        return true;
    }

    /**
     * @param $cart
     * @param int $productId
     * @return bool
     */
    protected function isAlreadyInBasket($cart, $productId)
    {
        foreach ($cart as $product) {
            if ($productId == $product) {
                return true;
            }
        }

        return false;
    }
}
