<?php

namespace CartBundle\Model;

use AppBundle\Entity\Product;
use CartBundle\Entity\Order;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\User;

class OrderModel
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function getEmptyOrder()
    {
        return new Order();
    }

    public function createOrder($user, $products)
    {
        $orders = $this->prepareOrders($user, $products);

        foreach ($orders as $order) {
            $this->em->persist($order);
        }

        $this->em->flush();

        return true;
    }

    /**
     * @param $user
     * @return array|\CartBundle\Entity\Order[]
     */
    public function getSellerOrders($user)
    {
        return $this->em->getRepository('CartBundle:Order')->findBy(['seller' => $user]);
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function getSellerOrdersCount($user)
    {
        $qb = $this->em->createQueryBuilder();

        $qb->select($qb->expr()->count('o'))
            ->from('CartBundle:Order', 'o')
            ->where($qb->expr()->eq('o.seller', $user->getId()));

        return $qb->getQuery()->getSingleScalarResult();
    }

    protected function prepareOrders($user, $products)
    {
        $orders = [];

        foreach ($products as $product) {
            /** @var Product $product */
            $currentSeller = $product->getUser();

            if (empty($orders[$currentSeller->getId()])) {
                $order = $this->getEmptyOrder();
                $order->setUser($user);
                $order->setSeller($currentSeller);
                $orders[$currentSeller->getId()] = $order;
            }

            /** @var Order[] $orders */
            $orders[$currentSeller->getId()]->setProduct($product);
        }

        return $orders;
    }
}
