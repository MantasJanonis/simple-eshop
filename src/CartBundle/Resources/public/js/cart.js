$(document).ready(function(){
    var thisController = this;

    this.init = function() {
        this.loadCart();
        this.loadBuyRequests();
        this.setToCartEvents();
    };

    this.loadCart = function() {
        $.ajax({
            url: '/get-cart-icon/',
            success: function(data) {
                $(data).insertAfter('nav.navbar');
            }
        });
    };

    this.loadBuyRequests = function() {
        if ($('#login').length == 0) {
            $.ajax({
                url: '/get-buy-requests/',
                success: function (data) {
                    $('ul.nav').prepend(data);
                }
            });
        }
    };

    this.refreshCartIcon = function() {
        $.ajax({
            url: '/get-cart-icon/',
            success: function(data) {
                $('#cart-icon').html(data);
            }
        });
    };

    this.setToCartEvents = function() {
        $('.add-to-cart').click(function(e) {
            var productId = $(e.target).data('product-id');
            $.post({
                url: '/add-to-cart/',
                data: {
                    productId: productId
                },
                success: function() {
                    thisController.refreshCartIcon()
                }
            })
        });
    };

    this.init();
});
