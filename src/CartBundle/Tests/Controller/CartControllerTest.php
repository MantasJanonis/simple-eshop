<?php

namespace CartBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CartControllerTest extends WebTestCase
{
    public function testMyCart()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/my-cart/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Your cart is empty. Browse to buy products', $crawler->filter('.alert')->text());
    }

    public function testGetCartIcon()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/get-cart-icon/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('cart.png', $crawler->filter('#cart-icon')->selectImage('Cart')->image()->getUri());
    }

    public function testCreateGuestOrder()
    {
        $client = static::createClient();

        $this->addToBasket($client);
        $this->goToBasket($client);

        $crawler = $client->request('GET', '/create-order/');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $this->assertContains('Redirecting to', $crawler->html());
        $this->assertContains('/login', $crawler->html());
    }

    protected function addToBasket($client)
    {
        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $productId = $crawler->filter('.add-to-cart')->getNode(0)->getAttribute('data-product-id');
        $this->assertGreaterThan(0, $productId);

        $client->request('POST', '/add-to-cart/', ['productId' => $productId]);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    protected function goToBasket($client)
    {
        $crawler = $client->request('GET', '/my-cart/');
        $this->assertContains('Total price', $crawler->filter('.cart-price')->text());
        $this->assertContains('Buy', $crawler->filter('.create-order-btn')->text());

        $url = $crawler->filter('.create-order-btn')->getNode(0)->getAttribute('href');
        $this->assertEquals('/create-order/', $url);
    }
}
