<?php

namespace CartBundle\Tests\Utils;

use CartBundle\Utils\PriceCalculator;
use AppBundle\Entity\Product;

class PriceCalculatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider productsProvider
     *
     * @param $source
     * @param $expectedResult
     */
    public function testCalculateTotalPrice($source, $expectedResult)
    {
        $priceCalculator = new PriceCalculator();
        $this->assertSame($expectedResult, $priceCalculator->calculateTotalPrice($source));
    }

    public function productsProvider()
    {
        $firstDataSource = [
            $this->getReadyProduct(25),
            $this->getReadyProduct(35),
            $this->getReadyProduct(40),
        ];
        $secondDataSource = [
            $this->getReadyProduct(21.12),
            $this->getReadyProduct(23.25),
            $this->getReadyProduct(40.25),
        ];
        $thirdDataSource = [
            $this->getReadyProduct('25.33'),
            $this->getReadyProduct(35.14),
            $this->getReadyProduct(40),
        ];

        return [
            [$firstDataSource, 100],
            [$secondDataSource, 84.62],
            [$thirdDataSource, 100.47],
        ];
    }

    public function getReadyProduct($price)
    {
        $product = new Product();
        $product->setPrice($price);

        return $product;
    }
}