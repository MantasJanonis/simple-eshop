<?php

namespace CartBundle\Utils;

use AppBundle\Entity\Product;

class PriceCalculator implements PriceCalculatorInterface
{
    /**
     * Calculates price of given products
     *
     * @param array $products
     * @return float
     */
    public function calculateTotalPrice($products)
    {
        $price = 0;
        foreach ($products as $product) {
            /** @var Product $product  */
            $price += $product->getPrice();
        }

        return $price;
    }
}