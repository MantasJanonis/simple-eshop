<?php

namespace CartBundle\Utils;


interface PriceCalculatorInterface
{
    /**
     * Calculates price of given products
     *
     * @param array $products
     * @return float
     */
    public function calculateTotalPrice($products);
}